Solo in .: filenostro.txt
diff -urp ../lb-orig/lb.c ./lb.c
--- ../lb-orig/lb.c	2016-06-30 14:14:38.000000000 +0200
+++ ./lb.c	2016-07-22 14:58:40.343719777 +0200
@@ -1,3 +1,5 @@
+
+
 /*
  * Copyright (C) 2016 Broala and Universita` di Pisa. All rights reserved.
  *
@@ -40,6 +42,8 @@
 
 #include "pkt_hash.h"
 #include "ctrs.h"
+#include "../../examples/ctrs.h"
+#include "../../sys/net/netmap_user.h"
 
 
 /*
@@ -85,6 +89,10 @@ struct compact_ipv6_hdr {
 #define DEF_BATCH	2048
 #define DEF_SYSLOG_INT	600
 #define BUF_REVOKE	100
+#define RX_TO_MULTI_TX 1
+#define MULTI_RX_TO_TX 2
+#define BIDIRECTIONAL 3 
+#define NOT_VALID_MODE 4					
 
 struct {
 	char ifname[MAX_IFNAMELEN];
@@ -135,18 +143,23 @@ oq_deq(struct overflow_queue *q)
 	return s;
 }
 
+
 static volatile int do_abort = 0;
 
 uint64_t dropped = 0;
 uint64_t forwarded = 0;
+uint64_t fpti_forwarded = 0;
+uint64_t fpti_dropped = 0;
 uint64_t non_ip = 0;
 
 struct port_des {
-	struct my_ctrs ctr;
+	struct my_ctrs iface_to_pipe_ctr;
+	struct my_ctrs pipe_to_iface_ctr;
 	unsigned int last_sync;
 	struct overflow_queue *oq;
 	struct nm_desc *nmd;
-	struct netmap_ring *ring;
+	struct netmap_ring * r_ring;
+	struct netmap_ring * t_ring;
 };
 
 struct port_des *ports;
@@ -157,25 +170,37 @@ print_stats(void *arg)
 	int npipes = glob_arg.output_rings;
 	int sys_int = 0;
 	(void)arg;
-	struct my_ctrs cur, prev;
+	struct my_ctrs iface_to_pipe_cur, iface_to_pipe_prev;
+	struct my_ctrs pipe_to_iface_prev;
 	char b1[40], b2[40];
-	struct my_ctrs *pipe_prev;
+	struct my_ctrs *forward_pipe_prev;
+	struct my_ctrs *backward_pipe_prev;
+	struct port_des *if_port = &ports[npipes];
 
-	pipe_prev = calloc(npipes, sizeof(struct my_ctrs));
-	if (pipe_prev == NULL) {
+	forward_pipe_prev = calloc(npipes, sizeof(struct my_ctrs));
+	if (forward_pipe_prev == NULL) {
 		D("out of memory");
 		exit(1);
 	}
 
-	memset(&prev, 0, sizeof(prev));
-	gettimeofday(&prev.t, NULL);
+	backward_pipe_prev = calloc(npipes, sizeof(struct my_ctrs));
+	if (backward_pipe_prev == NULL) {
+		D("out of memory");
+		exit(1);
+	}
+
+	memset(&iface_to_pipe_prev, 0, sizeof(iface_to_pipe_prev));
+	memset(&pipe_to_iface_prev, 0, sizeof(pipe_to_iface_prev));
+
+	gettimeofday(&iface_to_pipe_prev.t, NULL);
 	while (!do_abort) {
 		int j, dosyslog = 0;
 		uint64_t pps, dps, usec;
 		struct my_ctrs x;
 
-		memset(&cur, 0, sizeof(cur));
-		usec = wait_for_next_report(&prev.t, &cur.t, 1000);
+		memset(&iface_to_pipe_cur, 0, sizeof(iface_to_pipe_cur));
+
+		usec = wait_for_next_report(&iface_to_pipe_prev.t, &iface_to_pipe_cur.t, 1000);
 
 		if (++sys_int == glob_arg.syslog_interval) {
 			dosyslog = 1;
@@ -185,70 +210,161 @@ print_stats(void *arg)
 		for (j = 0; j < npipes; ++j) {
 			struct port_des *p = &ports[j];
 
-			cur.pkts += p->ctr.pkts;
-			cur.drop += p->ctr.drop;
+			iface_to_pipe_cur.pkts += p->iface_to_pipe_ctr.pkts;
+			iface_to_pipe_cur.drop += p->iface_to_pipe_ctr.drop;
+
+			// Forward direction ( interface to pipe )
+			x.pkts = p->iface_to_pipe_ctr.pkts - forward_pipe_prev[j].pkts;
+			x.drop = p->iface_to_pipe_ctr.drop - forward_pipe_prev[j].drop;
+			pps = (x.pkts*1000000 + usec/2) / usec;
+			dps = (x.drop*1000000 + usec/2) / usec;
+			printf("Pipe %d F: ",j);
+			printf("%s/%s|", norm(b1, pps), norm(b2, dps));
+			forward_pipe_prev[j] = p->iface_to_pipe_ctr;
+
+			if (dosyslog) {
+				syslog(LOG_INFO,
+					   "{"
+							   "\"interface\":\"%s\","
+							   "\"output_ring\":%"PRIu16","
+						"\"packets_forwarded\":%"PRIu64","
+						"\"packets_dropped\":%"PRIu64
+				"}", glob_arg.ifname, j, p->iface_to_pipe_ctr.pkts, p->iface_to_pipe_ctr.drop);
+			}
 
-			x.pkts = p->ctr.pkts - pipe_prev[j].pkts;
-			x.drop = p->ctr.drop - pipe_prev[j].drop;
+			// Backward direction ( pipe to interface )
+			x.pkts = p->pipe_to_iface_ctr.pkts - backward_pipe_prev[j].pkts;
+			x.drop = p->pipe_to_iface_ctr.drop - backward_pipe_prev[j].drop;
 			pps = (x.pkts*1000000 + usec/2) / usec;
 			dps = (x.drop*1000000 + usec/2) / usec;
+			printf("B: ");
 			printf("%s/%s|", norm(b1, pps), norm(b2, dps));
-			pipe_prev[j] = p->ctr;
+			backward_pipe_prev[j] = p->pipe_to_iface_ctr;
 
 			if (dosyslog) {
 				syslog(LOG_INFO,
-					"{"
-						"\"interface\":\"%s\","
-						"\"output_ring\":%"PRIu16","
+					   "{"
+							   "\"interface\":%"PRIu16","
+							   "\"output_ring\":\"%s\","
 						"\"packets_forwarded\":%"PRIu64","
 						"\"packets_dropped\":%"PRIu64
-					"}", glob_arg.ifname, j, p->ctr.pkts, p->ctr.drop);
+				"}", j, glob_arg.ifname, p->pipe_to_iface_ctr.pkts, p->pipe_to_iface_ctr.drop);
+
 			}
 		}
 		printf("\n");
 		if (dosyslog) {
 			syslog(LOG_INFO,
-				"{"
-					"\"interface\":\"%s\","
-					"\"output_ring\":null,"
-					"\"packets_forwarded\":%"PRIu64","
+				   "{"
+						   "\"interface\":\"%s\","
+						   "\"output_ring\":null,"
+						   "\"packets_forwarded\":%"PRIu64","
 					"\"packets_dropped\":%"PRIu64","
 					"\"non_ip_packets\":%"PRIu64
-				"}", glob_arg.ifname, forwarded, dropped, non_ip);
+			"}", glob_arg.ifname, forwarded, dropped, non_ip);
 		}
-		x.pkts = cur.pkts - prev.pkts;
-		x.drop = cur.drop - prev.drop;
+		
+		// Forward direction - count ingoing packets of the interface
+		x.pkts = iface_to_pipe_cur.pkts - iface_to_pipe_prev.pkts;
+		x.drop = iface_to_pipe_cur.drop - iface_to_pipe_prev.drop;
 		pps = (x.pkts*1000000 + usec/2) / usec;
 		dps = (x.drop*1000000 + usec/2) / usec;
 		printf("===> aggregate %spps %sdps\n", norm(b1, pps), norm(b2, dps));
-		prev = cur;
+		iface_to_pipe_prev = iface_to_pipe_cur;
+
+		// Backward direction - count outgoing packets of the interface
+		x.pkts = if_port->pipe_to_iface_ctr.pkts - pipe_to_iface_prev.pkts;
+		x.drop = if_port->pipe_to_iface_ctr.drop - pipe_to_iface_prev.drop;
+		pps = (x.pkts*1000000 + usec/2) / usec;
+		dps = (x.drop*1000000 + usec/2) / usec;
+		printf("<=== aggregate %spps %sdps\n", norm(b1, pps), norm(b2, dps));
+		pipe_to_iface_prev = if_port->pipe_to_iface_ctr;
+
 	}
 
-	free(pipe_prev);
+	free(forward_pipe_prev);
+	free(backward_pipe_prev);
 
 	return NULL;
 }
 
+
+// This function allows to swap a certain slot from netmap ring (s) to another one 
+// belonging to the pool of rings of netmap descriptor (d)
+static int
+nm_inject_from_ring(struct nm_desc *d, struct netmap_ring* s, int slot_i)
+{
+	u_int c, n = d->last_tx_ring - d->first_tx_ring + 1;
+
+	// Iterate over all the transmission rings, breaking when it's reached a free slot
+	for (c = 0; c < n ; c++) {
+		
+		/* compute current ring to use */
+		struct netmap_ring *ring;
+		uint32_t i, idx;
+		uint32_t ri = d->cur_tx_ring + c;
+
+		if (ri > d->last_tx_ring)
+			ri = d->first_tx_ring;
+
+		ring = NETMAP_TXRING(d->nifp, ri);
+
+		if (nm_ring_empty(ring)) {
+			continue;
+		}
+		i = ring->cur;
+		idx = ring->slot[i].buf_idx;
+		
+		// Copy len of data
+		ring->slot[i].len = s->slot[slot_i].len;
+		
+		// Swap buffer indexes
+		ring->slot[i].buf_idx = s->slot[slot_i].buf_idx;
+		s->slot[slot_i].buf_idx = idx;
+		
+		// Signal buffer changing
+		s->slot[slot_i].flags |= NS_BUF_CHANGED;
+		ring->slot[i].flags |= NS_BUF_CHANGED;
+		
+		// Update ring pointers
+		ring->head = ring->cur = nm_ring_next(ring, i);
+		s->head = s->cur = nm_ring_next(s,slot_i);
+		
+		// Set current tx ring
+		d->cur_tx_ring = ri;
+		
+		return 1;
+	}
+	
+	return 0; /* fail */ 
+}
+
 static void
 free_buffers(void)
 {
 	int i, tot = 0;
-	struct port_des *rxport = &ports[glob_arg.output_rings];
+	struct port_des *if_port = &ports[glob_arg.output_rings];
 
 	/* build a netmap free list with the buffers in all the overflow queues */
 	for (i = 0; i < glob_arg.output_rings + 1; i++) {
 		struct port_des *cp = &ports[i];
 		struct overflow_queue *q = cp->oq;
-
+		uint32_t *b;
+		
 		if (!q)
 			continue;
 
 		while (q->n) {
 			struct netmap_slot s = oq_deq(q);
-			uint32_t *b = (uint32_t *)NETMAP_BUF(cp->ring, s.buf_idx);
+			
+			// Substitution of generic ring with the correct rx/tx ring
+			if(i != glob_arg.output_rings) 
+				b = (uint32_t *)NETMAP_BUF(cp->t_ring, s.buf_idx);
+			else 
+				b = (uint32_t *)NETMAP_BUF(cp->r_ring, s.buf_idx);
 
-			*b = rxport->nmd->nifp->ni_bufs_head;
-			rxport->nmd->nifp->ni_bufs_head = s.buf_idx;
+			*b = if_port->nmd->nifp->ni_bufs_head;
+			if_port->nmd->nifp->ni_bufs_head = s.buf_idx;
 			tot++;
 		}
 	}
@@ -280,12 +396,213 @@ void usage()
 	exit(0);
 }
 
+/* Choose mode of operation of the load balancer: unidirectional or bidirectional */
+static void choose_mode(char* if_name, int *op_mode){
+
+	char* p = index(if_name, '/');
+	
+	// No specified mode means bidirectional
+	if(p == NULL){
+		*op_mode = BIDIRECTIONAL;
+	}
+	else{
+		p++;
+		if(*p == 'R'){
+			*op_mode = RX_TO_MULTI_TX;
+		}
+		else if(*p == 'T'){
+			*op_mode = MULTI_RX_TO_TX;
+		}
+		else{
+			*op_mode = NOT_VALID_MODE;
+		}
+	}
+
+}
+
+/* Compose correct pipe name starting from interface name and mode */
+static void npipe_name(char *name, int op_mode, int ifnum){
+
+	if (op_mode == NOT_VALID_MODE){
+		exit(1);
+	}
+
+	char *prefix_name;
+	char flag[3];
+	const char *c;
+	int i = 0;
+
+	for (c = glob_arg.ifname; *c && !index("/", *c); c++){
+		i++;
+	}
+
+	if (op_mode == RX_TO_MULTI_TX)
+		strcpy(flag, "/T");
+	else if (op_mode == MULTI_RX_TO_TX)
+		strcpy(flag, "/R");
+	
+	flag[2] = '\0';
+
+	prefix_name = malloc(i+1);
+	strncpy(prefix_name, glob_arg.ifname, i);
+	prefix_name[i+1] = '\0';
+
+	sprintf(name, "%s{%d%s", prefix_name, ifnum, flag);
+
+}
+
+/* Used to set the ring pointers in port_des */
+static void set_ring_mode(struct port_des *port, int op_mode, int is_a_pipe){
+
+	switch(op_mode){
+
+		case BIDIRECTIONAL:
+
+			port->r_ring = NETMAP_RXRING(port->nmd->nifp, 0);
+			port->t_ring = NETMAP_TXRING(port->nmd->nifp, 0);
+			break;
+
+		case RX_TO_MULTI_TX:
+
+			port->r_ring = (is_a_pipe)? NULL : NETMAP_RXRING(port->nmd->nifp, 0);
+			port->t_ring = (is_a_pipe)? NETMAP_TXRING(port->nmd->nifp, 0) : NULL;
+			break;
+
+		case MULTI_RX_TO_TX:
+
+			port->r_ring = (is_a_pipe)? NETMAP_RXRING(port->nmd->nifp, 0) : NULL;
+			port->t_ring = (is_a_pipe)? NULL : NETMAP_TXRING(port->nmd->nifp, 0);
+			break;
+
+		default:
+			D("Operative mode not valid!");
+			exit(1);
+
+	}
+
+}
+
+
+/* Thread calling nm_inject_from_ring()*/
+static void *thread_code_pipes_to_interface(){
+
+	D("Pipe-to-interface thread created successfully\n");
+
+	unsigned int iter = 0;
+	int npipes = glob_arg.output_rings;
+	struct port_des *if_port = &ports[npipes];
+	
+	struct pollfd pollfd[npipes + 1];
+	memset(&pollfd, 0, sizeof(pollfd));
+
+	int next_port = 0;
+	int i, j;
+	
+	while (!do_abort) {
+		u_int polli = 0;
+		iter++;
+
+		uint8_t not_full = false;
+
+		// Check if if_port is full (all rings!)
+		for (i = if_port->nmd->first_tx_ring; i <= if_port->nmd->last_tx_ring; i++){
+			struct netmap_ring *txring = NETMAP_TXRING(if_port->nmd->nifp, i);
+			if (nm_ring_space(txring)) {
+				not_full = true;
+				break;
+			}
+		}
+
+		if (not_full) {
+
+			// Register all the pipes (POLLIN)
+			for (i = 0; i < npipes; ++i) {
+				pollfd[polli].fd = ports[i].nmd->fd;
+				pollfd[polli].events = POLLIN;
+				pollfd[polli].revents = 0;
+				++polli;
+			}
+		}
+
+		// Register if_port for POLLOUT event
+		pollfd[polli].fd = if_port->nmd->fd;
+		pollfd[polli].events = POLLOUT;
+		pollfd[polli].revents = 0;
+		++polli;
+
+		//RD(5, "polling %d file descriptors", polli+1);
+		i = poll(pollfd, polli, 10);
+
+		if (i <= 0) {
+			RD(1, "poll error %s", errno ? strerror(errno) : "timeout");
+			continue;
+		} else {
+			//RD(5, "Poll returned %d", i);
+		}
+
+		int batch = 0;
+		
+		// Check pipes for new arrived packets in slots. 
+		// Start from the following pipe w.r.t the last cycle checked pipe (next_port)
+		for(j = 0; j < npipes; j++){
+
+			int index = (j + next_port) % npipes;
+
+			struct port_des *pipe = &ports[index];
+
+			struct netmap_ring *rxring = pipe->r_ring;
+
+			// forward all the packets for the current pipe
+			while (!nm_ring_empty(rxring)) {
+
+				int ret;
+				next_port = (index + 1) % npipes;
+				j = 0;
+
+				// forward packet (injection of the pipe-RX-ring slot to the first free if_port-TX-ring slot)
+				ret = nm_inject_from_ring(if_port->nmd, rxring, rxring->cur);
+
+				// Update Counters
+				if (ret == 1) {
+					if_port->pipe_to_iface_ctr.pkts++;
+					pipe->pipe_to_iface_ctr.pkts++;
+					fpti_forwarded++;
+					
+				} else {
+					if_port->pipe_to_iface_ctr.drop++;
+					pipe->pipe_to_iface_ctr.drop++;
+					fpti_dropped++;
+				}
+
+				/* Use overflow queue, if available. Current version of lb hasn't the overflow queue in backward direction */
+
+				batch++;
+				if (unlikely(batch >= glob_arg.batch)) {
+					// Sync TX rings for interface port 
+					ioctl(if_port->nmd->fd, NIOCTXSYNC, NULL);
+					batch = 0;
+				}
+			}  
+
+			// Sync RX ring for pipe after reading
+		    ioctl(pipe->nmd->fd, NIOCRXSYNC, NULL);
+
+		}  
+		
+	}  
+
+	return NULL;
+}
+
+
+
 
 
 int main(int argc, char **argv)
 {
 	int ch;
 	uint32_t i;
+	int op_mode;
 	int rv;
 	unsigned int iter = 0;
 
@@ -361,7 +678,11 @@ int main(int argc, char **argv)
 		D("failed to allocate the stats array");
 		return 1;
 	}
-	struct port_des *rxport = &ports[npipes];
+	
+	choose_mode(glob_arg.ifname, &op_mode);
+	
+	// The interface port specified by the user
+	struct port_des *if_port = &ports[npipes];
 
 	if (pthread_create(&stat_thread, NULL, print_stats, NULL) == -1) {
 		D("unable to create the stats thread: %s", strerror(errno));
@@ -376,20 +697,23 @@ int main(int argc, char **argv)
 	base_req.nr_arg1 = npipes;
 	base_req.nr_arg3 = glob_arg.extra_bufs;
 
-	rxport->nmd = nm_open(glob_arg.ifname, &base_req, 0, NULL);
+	if_port->nmd = nm_open(glob_arg.ifname, &base_req, 0, NULL);
 
-	if (rxport->nmd == NULL) {
+	if (if_port->nmd == NULL) {
 		D("cannot open %s", glob_arg.ifname);
 		return (1);
 	} else {
 		D("successfully opened %s (tx rings: %u)", glob_arg.ifname,
-		  rxport->nmd->req.nr_tx_slots);
+		  if_port->nmd->req.nr_tx_rings);
+		D("successfully opened %s (rx rings: %u)", glob_arg.ifname,
+		if_port->nmd->req.nr_rx_rings);
 	}
 
-	uint32_t extra_bufs = rxport->nmd->req.nr_arg3;
+	uint32_t extra_bufs = if_port->nmd->req.nr_arg3;
 	struct overflow_queue *oq = NULL;
+
 	/* reference ring to access the buffers */
-	rxport->ring = NETMAP_RXRING(rxport->nmd->nifp, 0);
+	set_ring_mode(if_port, op_mode, 0);
 
 	if (!glob_arg.extra_bufs)
 		goto run;
@@ -408,7 +732,7 @@ int main(int argc, char **argv)
 	}
 
 	freeq = &oq[npipes];
-	rxport->oq = freeq;
+	if_port->oq = freeq;
 
 	freeq->slots = calloc(extra_bufs, sizeof(struct netmap_slot));
 	if (!freeq->slots) {
@@ -421,10 +745,11 @@ int main(int argc, char **argv)
 	 * the list of buffers uses the first uint32_t in each buffer
 	 * as the index of the next buffer.
 	 */
+
 	uint32_t scan;
-	for (scan = rxport->nmd->nifp->ni_bufs_head;
+	for (scan = if_port->nmd->nifp->ni_bufs_head;
 	     scan;
-	     scan = *(uint32_t *)NETMAP_BUF(rxport->ring, scan))
+	     scan = *(uint32_t *)NETMAP_BUF(if_port->r_ring, scan))
 	{
 		struct netmap_slot s;
 		s.buf_idx = scan;
@@ -439,26 +764,33 @@ int main(int argc, char **argv)
 				extra_bufs, freeq->n);
 		return 1;
 	}
-	rxport->nmd->nifp->ni_bufs_head = 0;
+	if_port->nmd->nifp->ni_bufs_head = 0;
 
+	
+	
 run:
 	for (i = 0; i < npipes; ++i) {
 		char interface[25];
-		sprintf(interface, "%s{%d", glob_arg.ifname, i);
+		npipe_name(interface, op_mode, i);
 		D("opening pipe named %s", interface);
 
-		ports[i].nmd = nm_open(interface, NULL, 0, rxport->nmd);
+		//ports[i].nmd = nm_open(interface, NULL, NM_OPEN_NO_MMAP | NM_OPEN_ARG3 | NM_OPEN_RING_CFG, if_port->nmd);
+		ports[i].nmd = nm_open(interface, NULL, 0, if_port->nmd);
 
 		if (ports[i].nmd == NULL) {
 			D("cannot open %s", interface);
 			return (1);
 		} else {
-			D("successfully opened pipe #%d %s (tx slots: %d)",
-			  i + 1, interface, ports[i].nmd->req.nr_tx_slots);
-			ports[i].ring = NETMAP_TXRING(ports[i].nmd->nifp, 0);
+			if(op_mode == BIDIRECTIONAL || op_mode == RX_TO_MULTI_TX) 
+				D("successfully opened pipe #%d %s (tx slots: %d)",i + 1, interface, ports[i].nmd->req.nr_tx_slots);
+			if(op_mode == BIDIRECTIONAL || op_mode == MULTI_RX_TO_TX) 
+				D("successfully opened pipe #%d %s (rx slots: %d)",i + 1, interface, ports[i].nmd->req.nr_rx_slots);			
+			
+			set_ring_mode(&ports[i], op_mode, 1);
+			
 		}
 		D("zerocopy %s",
-		  (rxport->nmd->mem == ports[i].nmd->mem) ? "enabled" : "disabled");
+		  (if_port->nmd->mem == ports[i].nmd->mem) ? "enabled" : "disabled");
 
 		if (extra_bufs) {
 			struct overflow_queue *q = &oq[i];
@@ -488,172 +820,203 @@ run:
 
 	sleep(2);
 
-	struct pollfd pollfd[npipes + 1];
-	memset(&pollfd, 0, sizeof(pollfd));
-
 	signal(SIGINT, sigint_h);
-	while (!do_abort) {
-		u_int polli = 0;
-		iter++;
 
-		for (i = 0; i < npipes; ++i) {
-			struct netmap_ring *ring = ports[i].ring;
-			if (nm_ring_next(ring, ring->tail) == ring->cur) {
-				/* no need to poll, there are no packets pending */
-				continue;
-			}
-			pollfd[polli].fd = ports[i].nmd->fd;
-			pollfd[polli].events = POLLOUT;
-			pollfd[polli].revents = 0;
-			++polli;
-		}
+	pthread_t pipes_to_if;
 
-		pollfd[polli].fd = rxport->nmd->fd;
-		pollfd[polli].events = POLLIN;
-		pollfd[polli].revents = 0;
-		++polli;
+	if(op_mode ==  MULTI_RX_TO_TX || op_mode == BIDIRECTIONAL){  
 
-		//RD(5, "polling %d file descriptors", polli+1);
-		rv = poll(pollfd, polli, 10);
-		if (rv <= 0) {
-			if (rv < 0 && errno != EAGAIN && errno != EINTR)
-				RD(1, "poll error %s", strerror(errno));
-			continue;
+		// enable backward direction management for the load balancer
+		if (pthread_create(&pipes_to_if, NULL, thread_code_pipes_to_interface, NULL) == -1) {
+			D("unable to create pipe-to-interface thread: %s", strerror(errno));
+			return 1;
 		}
 
-		if (oq) {
-			/* try to push packets from the overflow queues
-			 * to the corresponding pipes
-			 */
-			for (i = 0; i < npipes; i++) {
-				struct port_des *p = &ports[i];
-				struct overflow_queue *q = p->oq;
-				uint32_t j, lim;
-				struct netmap_ring *ring;
-				struct netmap_slot *slot;
 
-				if (!q->n)
-					continue;
-				ring = p->ring;
-				lim = nm_ring_space(ring);
-				if (!lim)
+
+
+	}
+	
+	
+	if(op_mode ==  RX_TO_MULTI_TX || op_mode == BIDIRECTIONAL){ 
+
+		struct pollfd pollfd[npipes + 1];
+		memset(&pollfd, 0, sizeof(pollfd));
+
+
+		while (!do_abort) {
+			u_int polli = 0;
+			iter++;
+
+			for (i = 0; i < npipes; ++i) {
+				struct netmap_ring *ring = ports[i].t_ring;
+				if (nm_ring_next(ring, ring->tail) == ring->cur) {
+					/* no need to poll, there are no packets pending */
 					continue;
-				if (q->n < lim)
-					lim = q->n;
-				for (j = 0; j < lim; j++) {
-					struct netmap_slot s = oq_deq(q);
-					slot = &ring->slot[ring->cur];
-					oq_enq(freeq, slot);
-					*slot = s;
-					slot->flags |= NS_BUF_CHANGED;
-					ring->cur = nm_ring_next(ring, ring->cur);
 				}
-				ring->head = ring->cur;
-				forwarded += lim;
-				p->ctr.pkts += lim;
+				pollfd[polli].fd = ports[i].nmd->fd;
+				pollfd[polli].events = POLLOUT;
+				pollfd[polli].revents = 0;
+				++polli;
 			}
-		}
 
-		int batch = 0;
-		for (i = rxport->nmd->first_rx_ring; i <= rxport->nmd->last_rx_ring; i++) {
-			struct netmap_ring *rxring = NETMAP_RXRING(rxport->nmd->nifp, i);
+			pollfd[polli].fd = if_port->nmd->fd;
+			pollfd[polli].events = POLLIN;
+			pollfd[polli].revents = 0;
+			++polli;
 
-			//D("prepare to scan rings");
-			int next_cur = rxring->cur;
-			struct netmap_slot *next_slot = &rxring->slot[next_cur];
-			const char *next_buf = NETMAP_BUF(rxring, next_slot->buf_idx);
-			while (!nm_ring_empty(rxring)) {
-				struct overflow_queue *q;
-				struct netmap_slot *rs = next_slot;
+			//RD(5, "polling %d file descriptors", polli+1);
+			rv = poll(pollfd, polli, 10);
+			if (rv <= 0) {
+				if (rv < 0 && errno != EAGAIN && errno != EINTR)
+					RD(1, "poll error %s", strerror(errno));
+				continue;
+			}
 
-				// CHOOSE THE CORRECT OUTPUT PIPE
-				uint32_t hash = pkt_hdr_hash((const unsigned char *)next_buf, 4, 'B');
-				if (hash == 0)
-					non_ip++; // XXX ??
-				// prefetch the buffer for the next round
-				next_cur = nm_ring_next(rxring, next_cur);
-				next_slot = &rxring->slot[next_cur];
-				next_buf = NETMAP_BUF(rxring, next_slot->buf_idx);
-				__builtin_prefetch(next_buf);
-				// 'B' is just a hashing seed
-				uint32_t output_port = hash % glob_arg.output_rings;
-				struct port_des *port = &ports[output_port];
-				struct netmap_ring *ring = port->ring;
-				uint32_t free_buf;
-
-				// Move the packet to the output pipe.
-				if (nm_ring_space(ring)) {
-					struct netmap_slot *ts = &ring->slot[ring->cur];
-					free_buf = ts->buf_idx;
-					ts->buf_idx = rs->buf_idx;
-					ts->len = rs->len;
-					ts->flags |= NS_BUF_CHANGED;
-					ring->head = ring->cur = nm_ring_next(ring, ring->cur);
-					port->ctr.pkts++;
-					forwarded++;
-					goto forward;
+			if (oq) {
+				/* try to push packets from the overflow queues
+				 * to the corresponding pipes
+				 */
+				for (i = 0; i < npipes; i++) {
+					struct port_des *p = &ports[i];
+					struct overflow_queue *q = p->oq;
+					uint32_t j, lim;
+					struct netmap_ring *ring;
+					struct netmap_slot *slot;
+
+					if (!q->n)
+						continue;
+					ring = p->t_ring;
+					lim = nm_ring_space(ring);
+					if (!lim)
+						continue;
+					if (q->n < lim)
+						lim = q->n;
+					for (j = 0; j < lim; j++) {
+						struct netmap_slot s = oq_deq(q);
+						slot = &ring->slot[ring->cur];
+						oq_enq(freeq, slot);
+						*slot = s;
+						slot->flags |= NS_BUF_CHANGED;
+						ring->cur = nm_ring_next(ring, ring->cur);
+					}
+					ring->head = ring->cur;
+					forwarded += lim;
+					p->iface_to_pipe_ctr.pkts += lim;
 				}
+			}
 
-				/* use the overflow queue, if available */
-				if (!oq) {
-					dropped++;
-					port->ctr.drop++;
-					goto next;
-				}
+			int batch = 0;
+			for (i = if_port->nmd->first_rx_ring; i <= if_port->nmd->last_rx_ring; i++) {
+				struct netmap_ring *rxring = NETMAP_RXRING(if_port->nmd->nifp, i);
+
+				//D("prepare to scan rings");
+				int next_cur = rxring->cur;
+				struct netmap_slot *next_slot = &rxring->slot[next_cur];
+				const char *next_buf = NETMAP_BUF(rxring, next_slot->buf_idx);
+				while (!nm_ring_empty(rxring)) {
+					struct overflow_queue *q;
+					struct netmap_slot *rs = next_slot;
+
+					// CHOOSE THE CORRECT OUTPUT PIPE
+					uint32_t hash = pkt_hdr_hash((const unsigned char *)next_buf, 4, 'B');
+					if (hash == 0)
+						non_ip++; // XXX ??
+					// prefetch the buffer for the next round
+					next_cur = nm_ring_next(rxring, next_cur);
+					next_slot = &rxring->slot[next_cur];
+					next_buf = NETMAP_BUF(rxring, next_slot->buf_idx);
+					__builtin_prefetch(next_buf);
+					// 'B' is just a hashing seed
+					uint32_t output_port = hash % glob_arg.output_rings;
+					struct port_des *port = &ports[output_port];
+					struct netmap_ring *ring = port->t_ring;
+					uint32_t free_buf;
+
+					// Move the packet to the output pipe.
+					if (nm_ring_space(ring)) {
+						struct netmap_slot *ts = &ring->slot[ring->cur];
+						free_buf = ts->buf_idx;
+						ts->buf_idx = rs->buf_idx;
+						ts->len = rs->len;
+						ts->flags |= NS_BUF_CHANGED;
+						ring->head = ring->cur = nm_ring_next(ring, ring->cur);
+						port->iface_to_pipe_ctr.pkts++;
+						forwarded++;
+						goto forward;
+					}
+
+					/* use the overflow queue, if available */
+					if (!oq) {
+						dropped++;
+						port->iface_to_pipe_ctr.drop++;
+						goto next;
+					}
 
-				q = &oq[output_port];
+					q = &oq[output_port];
 
-				if (!freeq->n) {
-					/* revoke some buffers from the longest overflow queue */
-					uint32_t j;
-					struct port_des *lp = &ports[0];
-					uint32_t max = lp->oq->n;
-
-					for (j = 1; j < npipes; j++) {
-						struct port_des *cp = &ports[j];
-						if (cp->oq->n > max) {
-							lp = cp;
-							max = cp->oq->n;
+					if (!freeq->n) {
+						/* revoke some buffers from the longest overflow queue */
+						uint32_t j;
+						struct port_des *lp = &ports[0];
+						uint32_t max = lp->oq->n;
+
+						for (j = 1; j < npipes; j++) {
+							struct port_des *cp = &ports[j];
+							if (cp->oq->n > max) {
+								lp = cp;
+								max = cp->oq->n;
+							}
 						}
-					}
 
-					// XXX optimize this cycle
-					for (j = 0; lp->oq->n && j < BUF_REVOKE; j++) {
-						struct netmap_slot tmp = oq_deq(lp->oq);
-						oq_enq(freeq, &tmp);
-					}
+						// XXX optimize this cycle
+						for (j = 0; lp->oq->n && j < BUF_REVOKE; j++) {
+							struct netmap_slot tmp = oq_deq(lp->oq);
+							oq_enq(freeq, &tmp);
+						}
 
-					ND(1, "revoked %d buffers from %s", j, lq->name);
-					lp->ctr.drop += j;
-					dropped += j;
-				}
+						ND(1, "revoked %d buffers from %s", j, lq->name);
+						lp->iface_to_pipe_ctr.drop += j;
+						dropped += j;
+					}
 
-				free_buf = oq_deq(freeq).buf_idx;
-				oq_enq(q, rs);
+					free_buf = oq_deq(freeq).buf_idx;
+					oq_enq(q, rs);
 
-			forward:
-				rs->buf_idx = free_buf;
-				rs->flags |= NS_BUF_CHANGED;
-			next:
-				rxring->head = rxring->cur = next_cur;
+					forward:
+					rs->buf_idx = free_buf;
+					rs->flags |= NS_BUF_CHANGED;
+					next:
+					rxring->head = rxring->cur = next_cur;
+
+					batch++;
+					if (unlikely(batch >= glob_arg.batch)) {
+						ioctl(if_port->nmd->fd, NIOCRXSYNC, NULL);
+						batch = 0;
+					}
+					ND(1,
+					   "Forwarded Packets: %"PRIu64" Dropped packets: %"PRIu64"   Percent: %.2f",
+							forwarded, dropped,
+							((float)dropped / (float)forwarded * 100));
+					}
 
-				batch++;
-				if (unlikely(batch >= glob_arg.batch)) {
-					ioctl(rxport->nmd->fd, NIOCRXSYNC, NULL);
-					batch = 0;
 				}
-				ND(1,
-				   "Forwarded Packets: %"PRIu64" Dropped packets: %"PRIu64"   Percent: %.2f",
-				   forwarded, dropped,
-				   ((float)dropped / (float)forwarded * 100));
 			}
 
 		}
-	}
+
+		// start the thread pipes to if
+
 
 	pthread_join(stat_thread, NULL);
+	if(op_mode ==  MULTI_RX_TO_TX || op_mode == BIDIRECTIONAL)   
+		pthread_join(pipes_to_if, NULL);
 
-	printf("%"PRIu64" packets forwarded.  %"PRIu64" packets dropped. Total %"PRIu64"\n", forwarded,
-	       dropped, forwarded + dropped);
+
+		
+	printf("%"PRIu64" packets forwarded.  %"PRIu64" packets dropped. Total %"PRIu64"\n "
+			"%"PRIu64" packets fpti forwarded.  %"PRIu64" packets fpti dropped. Total %"PRIu64"\n ", forwarded,
+	       dropped, forwarded + dropped, fpti_forwarded, fpti_dropped, fpti_forwarded + fpti_dropped);
 	return 0;
 }
+
Solo in .: start_load_balancer_backward.sh
Solo in .: start_load_balancer_bidirectional.sh
Solo in .: start_load_balancer_forward.sh
